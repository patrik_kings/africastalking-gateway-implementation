from flask import Flask, render_template, request, config, logging, make_response
from main.Send_Message import SendMessage
from main.Make_Calls import  MakeCall
from main.Send_Airtime import SendAirtime
from main.User_Data import UserData


# CONFIG
DEBUG = True
SECRET_KEY = 'b86vhc345r687ygg34hf12cx465rt78igv657y897t6'

app = Flask(__name__)
app.config.from_object(__name__)


@app.route('/home')
def index():
    results = UserData()
    results = results.getuserdata()
    return render_template('userdata.html', results=results)


@app.route('/sms')
def sms():
    return render_template('sms.html')


@app.route('/sms/send', methods = ['POST'])
def sms_send():
    phone_number = request.form['phoneNumber']
    message = request.form['message']
    s = SendMessage(phone_number, message)
    results = s.send
    if type(results) == list:
        return render_template('sms.html', results= results)
    else:
        return render_template('sms.html', error= results)


@app.route('/voice')
def voice():
    return render_template('voice.html')


@app.route('/voice/call', methods = ['POST'])
def voice_call():
    caller = request.form['caller']
    receiver = request.form['receiver']
    rs = MakeCall(caller, receiver)
    results = rs.make_call()
    if type(results) == list:
        return render_template('voice.html', results= results)
    else:
        return render_template('voice.html',error= results)


@app.route('/ussd')
def ussd():
    return render_template('index.html')


@app.route('/airtime')
def airtime():
    return render_template('airtime.html')


@app.route('/airtime/send', methods = ['POST'])
def airtime_send():
    phone_number = request.form['phoneNumber']
    amount = request.form['amount']
    rs = SendAirtime(phone_number, amount)
    results = rs.send()
    if type(results) == list:
        return render_template('airtime.html', results= results)
    else:
        return render_template('airtime.html', error= results)
    

if __name__ == '__main__':
   
    app.run(host = '0.0.0.0', port = 8000)
