from __future__ import print_function
from __future__ import absolute_import
from africastalking.AfricasTalkingGateway import AfricasTalkingGateway, AfricasTalkingGatewayException
from ..models import insert_data, Sent_Messages
from . import username, apikey


class SendMessage:
    def __init__(self, phone_number, message):
        
        self.phone_numbers = phone_number  # number string or a string of numbers separated by a comma
        self.message = message  # a string
    
    @property
    def send(self):
        
        gateway = AfricasTalkingGateway(username, apikey)
        
        try:
            results = gateway.sendMessage(self.phone_numbers, self.message)

            for recipient in results:
                
                insert_data(Sent_Messages,
                            [
                                {
                                    'message_id': recipient['messageId'],
                                    'number': recipient['number'],
                                    'status': recipient['status'],
                                    'cost': recipient['cost']
                                }
                            ]
                            )
            return results
        except AfricasTalkingGatewayException as e:
            print('Encountered an error while sending: %s' % str(e))
            return str(e)
