from __future__ import print_function
from __future__ import absolute_import
from ..models import insert_data, Calls
from africastalking.AfricasTalkingGateway import AfricasTalkingGateway, AfricasTalkingGatewayException
from . import username, apikey


class MakeCall:
    def __init__(self, caller, receiver):
        self.caller = caller
        self.receiver = receiver
    
    def make_call(self):
        gateway = AfricasTalkingGateway(username, apikey)
        gateway.VoiceURLString = "https://voice.africastalking.com"
        
        try:
            results = gateway.call(self.caller, self.receiver)
            for recipient in results:
                insert_data(Calls,
                            [
                                {
                                    'message_id': recipient['messageId'],
                                    'number': recipient['number'],
                                    'status': recipient['status'],
                                    'cost': recipient['cost']
                                }
                            ]
                            )
            return results
        except AfricasTalkingGatewayException as e:
            print('Encountered an error while making the call: %s' % str(e))
            return str(e)
