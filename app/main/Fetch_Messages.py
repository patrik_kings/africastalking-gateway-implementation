from __future__ import absolute_import
from africastalking.AfricasTalkingGateway import AfricasTalkingGateway, AfricasTalkingGatewayException
from ..models import insert_data, Recvd_Messages
from . import username, apikey


class FetchMessanges:
    def __init__(self):
        pass
    
    def fetch_messanges(self):
        gateway = AfricasTalkingGateway(username, apikey)
        
        fl = open('lastReceivedId', 'r')
        lastReceivedId = fl.read()
        fl.close()
        
        try:
            while True:
                messages = gateway.fetchMessages(lastReceivedId)
                if len(messages) == 0:
                    break
                
                for message in messages:
                    insert_data(
                        Recvd_Messages,
                        [
                            {
                                'message_id': message['linKId'],
                                'Sender': message['from'],
                                'receiver': message['to'],
                                'message': message['text'],
                                'date': message['date'],
                                
                            }
                        ]
                    )
                
                return messages
            fl = open('lastReceivedId', 'w')
            fl.write(lastReceivedId)
            fl.close()
        
        except AfricasTalkingGatewayException, e:
            print 'Encountered an error while fetching messages: %s' % str(e)
            return str(e)
