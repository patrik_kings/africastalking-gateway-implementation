from africastalking.AfricasTalkingGateway import AfricasTalkingGateway, AfricasTalkingGatewayException
from . import username, apikey

class UserData:
    def __init__(self):
        pass
    
    def getuserdata(self):
        gateway = AfricasTalkingGateway(username, apikey)
        try:
            results = gateway.getUserData()
            results = results['balance']
                        
            return results
        except AfricasTalkingGatewayException as e:
            print str(e)
            return str(e)
