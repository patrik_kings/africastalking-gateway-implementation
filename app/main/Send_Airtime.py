from __future__ import absolute_import
from africastalking.AfricasTalkingGateway import AfricasTalkingGateway, AfricasTalkingGatewayException
from . import username, apikey
from ..models import insert_data, Airtime


class SendAirtime:
    def __init__(self, phone_number, amount):
        
        self.phone_number = str(phone_number)
        self.amount = str(amount)
        
        self.recipients = [
            {
                "phoneNumber": self.phone_number,
                "amount": self.amount
            }
        ]
    
    def send(self):
        gateway = AfricasTalkingGateway(username, apikey)
        
        try:
            responses = gateway.sendAirtime(self.recipients)
            for response in responses:
                insert_data(Airtime,
                            [
                                {
                                    'request_id': response['requestId'],
                                    'number': response['phoneNumber'],
                                    'amount': response['amount'],
                                    'status': response['status'],
                                    'discount': response['discount'],
                        
                                }
                            ]
                            )
            
            return responses
        except AfricasTalkingGatewayException, e:
            print 'Encountered an error while sending airtime: %s' % str(e)
            return str(e)
