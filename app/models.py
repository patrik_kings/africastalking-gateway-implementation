from sqlalchemy import *

import os

basedir = os.path.abspath(os.path.dirname(__file__))

DATABASE = 'sqlite.db'

DATABASE_URI = 'sqlite:///' + os.path.join(basedir, DATABASE)

engine = create_engine(DATABASE_URI, connect_args = {'check_same_thread': False}, echo = True)

conn = engine.connect()

metadata = MetaData()

Recvd_Messages = Table('recvd_messages', metadata,
                       Column('message_id', Integer()),
                       Column('Sender', String()),
                       Column('receiver', String()),
                       Column('message', String()),
                       Column('date', DateTime()),
                       )

Sent_Messages = Table('sent_messages', metadata,
                      Column('message_id', Integer()),
                      Column('number', String()),
                      Column('status', String()),
                      Column('cost', String()),
                      )

Airtime = Table('airtime', metadata,
                Column('request_id', Integer()),
                Column('number', String()),
                Column('amount', String()),
                Column('status', String()),
                Column('discount', DateTime()),
                )

Calls = Table('calls', metadata,
                      Column('message_id', Integer()),
                      Column('number', String()),
                      Column('status', String()),
                      Column('cost', String()),
                      )

if os.path.join(basedir, DATABASE) is None:
    metadata.create_all(engine)


# insert an array of data [{'id':32, 'name':'troy'},{'id':5, 'name':'kings'}]
def insert_data(table_name, vals):
    result = conn.execute(table_name.insert(), vals)
    return result


# select statement
def select_all(table_name):
    result = conn.execute(select([table_name]))
    return result.fetchall()


# query statement
def queries(sql_text, **kwargs):
    result = conn.execute(sql_text, **kwargs).fetchall()
    return result
